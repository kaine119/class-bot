require 'discordrb'
require 'pg'
require 'rufus-scheduler'
require 'date'

scheduler = Rufus::Scheduler.new

bot = Discordrb::Commands::CommandBot.new token: ENV['DISCORD_TOKEN'], client_id: ENV['DISCORD_CLIENT_ID'], prefix: '!'
GENERAL_CHANNEL = ENV['GENERAL_CHANNEL']

con = PG::Connection.new ENV['DATABASE_URL']

puts "Invite me at: #{bot.invite_url}"

## Welcome party
bot.member_join do |event|
	user = event.user
	pm_channel = user.pm
	server = event.server
	user_member = user.on(server)

	# TODO: change message to take server name from api
	name_prompt = bot.send_message pm_channel, """
Hello! Welcome to the class channel. I'm a bot, *beep boop*.
Before you get started, I need to ask some questions.
First, what's your name?
	"""

	# will be filled out as the await repeats.
	name = ""
	birthday = ""
	identity = ""
	current_prompt = :name # the name comes first
	bot.add_await("landing_#{user.id}".to_sym, Discordrb::Events::PrivateMessageEvent, channel: pm_channel, from: user) do |event|
		# puts "message coming in!"
		# puts "message: #{event.message.text}"
		# puts "name: #{name}"
		# puts "birthday: #{birthday}"

		# first the name
		if name.empty?
			name = event.message.text
			current_prompt = :birthday
			event.respond "Alright #{name}, when's your birthday (YYYY-MM-DD)? (e.g. `2001-01-01`) I'll remember to wish you a happpy birthday."
			next false
		elsif birthday.empty?
			birthday = event.message.text
			current_prompt = :identity
			event.respond "Lastly, are you a teacher or a student? Reply \"teacher\" or \"student\"."
			next false
		elsif identity.empty?
			identity = event.message.text
			con.exec_params("INSERT INTO Users (name, birthday, discord_id)
								VALUES ($1, $2::date, $3);", [name, birthday, user.id])
			role = server.roles.find { |role| role.name.downcase.include? identity }
			user_member.add_role(role)
			# TODO: change message to take server name from api
			event.respond "All done! Go back to the class server on the left and say hi!"
			next true
		end
	end
end

THUMBS_UP = "\u{1F44d}".freeze

## Acknowledgements
bot.command :acknowledge do |event, role|
	role_to_ack = event.server.roles.find { |target| target.id == role.gsub(/<|&|>|@/) { |match| "" }.to_i }
	required_acks = role_to_ack.members.count { |member| !member.bot_account? }

# 	sleep(2)
	message = event.respond "#{role}, please acknowledge with the #{THUMBS_UP} button below."
	sleep (1)
	message.create_reaction THUMBS_UP
	bot.add_await("ack_#{message.id}".to_sym, Discordrb::Events::ReactionAddEvent, emoji: THUMBS_UP) do |reaction_event|
		next false unless reaction_event.message.id == message.id
		current_acks = reaction_event.message.reactions[THUMBS_UP].count - 1 # not counting the bot's own reaction
		message.edit "\u26A0 Acknowledged by #{current_acks}/#{required_acks}. Use the #{THUMBS_UP} button below to acknowledge"
		next false unless current_acks >= required_acks
		message.edit "\u2705 Acknowledged by #{required_acks}/#{required_acks}"
	end
# 	nil
end

## homework
bot.command :homework do |first_event, command|
	author = first_event.message.author
	if command == "list"
		list = con.exec "SELECT title, duedate, subject FROM Homeworks WHERE (duedate > CURRENT_DATE) ORDER BY duedate ASC;"
		if list.ntuples == 0
			first_event << "Nothing here! \\o/"
		else
			sorted_by_subject = list.group_by { |x| x['subject'] }
			sorted_by_subject.each_pair do |subject, homeworks|
				first_event << "__***#{subject}***__"
				homeworks.each { |homework| first_event << "#{homework['title']}, due #{Date.strptime(homework['duedate'], "%F").strftime("%d %B")}" }
				first_event << "\n"
			end
		end
	elsif command == "add"
		# required params to fill out
		current_title = ""
		current_duedate = ""
		current_subject = ""

		# initial prompt
		first_event.respond "What's the task, and when is it due? Please use the format `Name of task (MM-DD)`"
		subjects = []
		subjects = con.exec "SELECT subject FROM Homeworks GROUP BY subject"
		
		# start a looping await.
		author.await :homework do |homework_prompt_event|
			response = homework_prompt_event.message.text
			# first prompt for current_title and current_duedate
			if current_title.empty?
				# response with title and date
				date_regex = /\(\d\d-\d\d\)$/
				if !(response =~ date_regex)
					first_event.respond "Please use the format `Name of task (MM-DD)`."
					next false
				end
				current_title = response.gsub(date_regex) { |match|  ""}
				current_duedate = "2018-" + response[date_regex, 0].gsub(/\(|\)/) { |match| "" }
				first_event.respond "What subject is this for? Reply with the number in brackets, or reply with a new subject."
				subjects.each_with_index { |sub, ind| first_event.respond "#{ind}: #{sub['subject']}" }
				next false
			elsif current_subject.empty?
				# response with either subject number or new subject name
				if response =~ /^\d*$/
					current_subject = subjects[response.to_i]['subject']
				else
					current_subject = response
				end
				# we should have everything to push into db.
				con.exec_params("INSERT INTO Homeworks (title, duedate, subject)
								  VALUES ($1, $2, $3);", [current_title, current_duedate, current_subject])
				first_event.respond "All done! Use `!homework list` to see current list."
			end
		end
	else
		first_event.respond "*Usage: `!homework (list | add)`*"
	end
	nil
end



bot.run :async
bot.sync

# Repeats every day at 7:00am Singapore Time
scheduler.cron '0 7 * * * Singapore' do
	# SQL Connection
	todays_birthdays = con.exec "SELECT name, discord_id, DATE_PART('year', CURRENT_DATE) - DATE_PART('year', birthday) AS age 
					FROM Users 
					WHERE DATE_PART('day', birthday) = DATE_PART('day', CURRENT_DATE) 
						AND DATE_PART('month', birthday) = DATE_PART('month', CURRENT_DATE);"

	todays_birthdays.each do |birthday|
		bot.send_message GENERAL_CHANNEL, "Hey @everyone, it's <@#{birthday['discord_id']}>'s birthday! Happy #{birthday['age']}th! \u{1f382}"
	end
end

scheduler.join